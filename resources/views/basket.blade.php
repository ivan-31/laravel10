@extends('site')

@section('content')
    <h2>Ваша корзина</h2>

    @if ($basket->products->count())
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Название</th>
                <th scope="col">Цена</th>
                <th scope="col">Количество</th>
                <th scope="col">Сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($basket->products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ number_format($product->price, 2, '.', '') }}</td>
                    <td>{{ $product->pivot->quantity }}</td>
                    <td>{{ number_format($product->price * $product->pivot->quantity, 2, '.', '') }}</td>
                </tr>
            @endforeach
            <tr><td colspan="3"><b>ИТОГО:</b></td><td><b>{{ $basket->amount }}</b></td></tr>
            </tbody>
        </table>
        <form action="{{ route('basket.saveorder') }}" method="get">
            @csrf
            <button type="submit" class="btn btn-success">Оформить заказ</button>
        </form>
    @else
        <p>Ваша корзина пуста</p>
    @endif
@endsection
