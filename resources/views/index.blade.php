@extends('site')

@section('content')
    <h2>Каталог товаров</h2>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Цена</th>
            <th scope="col">Количество</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
                <tr>
                    <form action="{{ route('basket.add', ['id' => $product->id]) }}" method="post">
                        @csrf
                        <td>{{ $product->name }}</td>
                        <td>{{ number_format($product->price, 2, '.', '') }}</td>
                        <td><input type="text" pattern="^[0-9]+$" name="quantity" value="1" size="10"></td>
                        <td><button type="submit" class="btn btn-success">Добавить в корзину</button></td>
                    </form>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
