@extends('site')

@section('content')
    <h2>Все заказы</h2>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @elseif (session('error'))
        <div class="alert alert-warning">
            {{ session('error') }}
        </div>
    @endif

    @if ($orders->count())
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Номер заказа</th>
                <th scope="col">Дата заказа</th>
                <th scope="col">Товары</th>
                <th scope="col">Сумма</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @php
                $sum = 0;
            @endphp
            @foreach ($orders as $order)
                @php
                    $sum += $order->amount;
                @endphp
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>
                        @foreach ($order->products as $product)
                            {{ $product->name }}<br>
                        @endforeach
                    </td>
                    <td>{{ $order->amount }}</td>
                    <td><form action="{{ route('order.delete', ['id' => $order->id]) }}">
                    @csrf
                    <td><button type="submit" class="btn  btn-warning">Удалить</button></td>
                    </form></td>
                </tr>
            @endforeach
            <tr><td colspan="3"><b>ИТОГО:</b></td><td><b>{{ $sum }}</b></td><td></td></tr>
            </tbody>
        </table>
    @else
        <p>Нет заказов</p>
    @endif
@endsection
