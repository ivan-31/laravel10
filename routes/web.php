<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BasketController;
use App\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', IndexController::class)
    ->name('index');
Route::get('/basket',  [BasketController::class, 'index'])
    ->name('basket.index');
Route::get('/basket/saveorder', [BasketController::class, 'saveOrder'])
    ->name('basket.saveorder');
Route::post('/basket/add/{id}', [BasketController::class, 'add'])
    ->where('id', '[0-9]+')
    ->name('basket.add');
Route::get('/orders', [OrderController::class, 'index'])
    ->name('orders');
Route::get('/order/delete/{id}', [OrderController::class, 'deleteItem'])
    ->where('id', '[0-9]+')->name('order.delete');

