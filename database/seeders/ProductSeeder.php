<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            ['name' => '27" Монитор Acer KB272HLHbi черный', 'price' => 8999.00],
            ['name' => '23.6" Монитор AOC M2470SWD23 черный', 'price' => 9999.99],
            ['name' => '23.8" Монитор Xiaomi Mi Desktop Monitor 1С черный', 'price' => 10000.00],
            ['name' => 'Мышь беспроводная Gembird MUSW-420', 'price' => 100.50],
            ['name' => 'Мышь проводная Aceline CM-710BU', 'price' => 190.00],
            ['name' => 'Мышь беспроводная Aceline WM-907BU', 'price' => 250.00],
            ['name' => '1359873 Клавиатура проводная Aceline K-504BU', 'price' => 499.00],
            ['name' => 'Клавиатура проводная DEXP KB-103', 'price' => 699.35],
            ['name' => 'Клавиатура беспроводная KEYRON Pegasus', 'price' => 1050.00],
        ]);
    }
}
