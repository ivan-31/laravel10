<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

class OrderController extends Controller
{
    public function index() : View
    {
        $orders = Order::all();
        return view('orders', compact('orders'));
    }

    public function deleteItem($id) : RedirectResponse
    {
        $order = Order::find($id);

        if ($order) {
            $order->delete();
            return redirect()
                ->route('orders')
                ->with('success', 'Заказ успешно удален');
        } else {
            return redirect()
                ->route('orders')
                ->with('error', 'Заказ не найден');
        }
    }
}
