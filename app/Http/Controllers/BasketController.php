<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Basket;
use App\Models\Order;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class BasketController extends Controller
{
    private $basket;

    public function __construct()
    {
        $this->getBasket();
    }

    private function getBasket() : void
    {
        $basketId = request()->cookie('basket_id');

        if (!empty($basketId)) {
            try {
                $this->basket = Basket::findOrFail($basketId);
            } catch (ModelNotFoundException $e) {
                $this->basket = Basket::create();
            }
        } else {
            $this->basket = Basket::create();
        }
        Cookie::queue('basket_id', $this->basket->id, 525600);
    }

    public function index() : View
    {
        $basket = $this->basket;
        return view('basket', compact('basket'));
    }

    public function add(Request $request, $id) : RedirectResponse
    {
        $quantity = $request->input('quantity') ?? 1;
        $this->basket->addProduct($id, $quantity);

        return redirect()
            ->route('index')
            ->with('success', 'Товар успешно добавлен в корзину');
    }

    public function saveOrder() : View
    {
        $basket = $this->basket;
        $order = Order::create([
            'amount' => $basket->amount
        ]);

        foreach ($basket->products as $product) {
            $order->products()->attach($product->id, ['quantity' => $product->pivot->quantity]);
        }

        // уничтожаем корзину
        $basket->delete();

        return view('checkout');
    }
}
