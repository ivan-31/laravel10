<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity');
    }

    public function addProduct($id, $count = 1)
    {
        if ($this->products->contains($id)) {
            $pivotRow = $this->products()->where('product_id', $id)->first()->pivot;
            $quantity = $pivotRow->quantity + $count;
            $pivotRow->update(['quantity' => $quantity]);
        } elseif ($count > 0) {
            $this->products()->attach($id, ['quantity' => $count]);
        }

        $this->refresh();
        $this->amount += $this->products->find($id)->price * $count;
        $this->touch();// Сохраняем модель, обновляем updated_at
    }
}
